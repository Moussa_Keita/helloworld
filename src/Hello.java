public class Hello
{
    private int language;

    // TODO
    // Ajouter autres langues
    // Anglais  : Hello
    // Espagnol : Hola
    // Russe    : Привет
    private static String[] greeting = { "Salut" };
    private static String[] englishGreeting = { "Hello" };
    private static String[] spanishGreeting = { "Hola" };
    private static String[] RussianGreeting = { "Привет" };

    // TODO
    // Ajouter autres langues
    // Anglais  : 1
    // Espagnol : 2
    // Russe    : 3
    private static String[] languages = { "0 - Français","1 - Anglais","2 - Espagnol", "3 - Russe"};


    public Hello(int language) {
        // TODO
        // Ajouter vérification d'index
        //     Si index invalide : retourner greeting Français
        if (language < 0 || language > languages.length)
            this.language = languages[0];
        else
            this.language = languages[language];
    }

    public String GetGreeting() {

        return greeting[this.language];
    }

    public static String[] getEnglishGreeting() {
        return englishGreeting[this.language];
    }

    public static String[] getSpanishGreeting() {
        return spanishGreeting[this.language];
    }

    public static String[] getRussianGreeting() {
        return RussianGreeting[this.language];
    }

    public static String[] GetAvailableLanguages() {

        return languages;
    }
}
